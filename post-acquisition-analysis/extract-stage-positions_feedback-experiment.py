﻿data_folder=r"D:\Users\Granita\FeedbackTest-20240319\20240319-175156\Zoom"

filename_regular_expression="Image--W*--P*-T*-out.czi"

from System.IO import Directory
from os import path

imageFiles = Directory.GetFiles(data_folder,filename_regular_expression)

positions_table=ZenTable("Positions Table")
Zen.Application.Documents.Add(positions_table)

positions_table.Columns.Add("FileName")
positions_table.Columns.Add("Stage_Position_X")
positions_table.Columns.Add("Stage_Position_Y")

row_index=-1
for imageFile in imageFiles:
    print imageFile
    print path.basename(imageFile)
    
    #positions_table.NewRow()
    #positions_table.Rows.
    row_index=row_index+1#positions_table.RowCount-1
    
    positions_table.SetValue(row_index,positions_table.Columns.IndexOf("FileName"), path.basename(imageFile))
    
    img=Zen.Application.LoadImage(imageFile)
    position=img.Metadata.StagePositionMicron
    img.Close()
    
    positions_table.SetValue(row_index,positions_table.Columns.IndexOf("Stage_Position_X"), position.X)
    positions_table.SetValue(row_index,positions_table.Columns.IndexOf("Stage_Position_Y"), position.Y)
    
positions_table.Save(path.join(data_folder,"Positions_data.csv"))