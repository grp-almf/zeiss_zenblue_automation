﻿from System.IO.Ports import SerialPort

import time

ser1 = SerialPort('COM3')
ser1.BaudRate=9600
ser1.DataBits=8
ser1.Open()

print(ser1.IsOpen)

for i in range(1,4):
    print('Iteration %d' %i)
    ser1.Write('start\r')
    #ser1.WriteLine('start\r')
    time.sleep(10)


ser1.Close()

raw_input('Press enter to continue: ')
