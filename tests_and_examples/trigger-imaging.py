﻿
print dir(Zen)

print Zen.createObjRef()

job = Zen.Acquisition.Experiments.GetByName('Marco-Dendra2-confocal')
print type (job)
print dir(job)


#from zeiss_zenblue_automation.utils import MicroscopeTrigger
#reload(MicroscopeTrigger)

#print Zen in dir()

#MicroscopeTrigger.runAcquisitionJobOffset(jobName='Marco-Dendra2-confocal',zen=Zen, dx=0, dy=0, dz=0)
#Zen.Acquisition.Experiments

def runAcquisitionJobOffset(jobName, dx, dy, dz):
    exp = Zen.Acquisition.Experiments.GetByName(jobName)
    print type (exp)
    print dir (exp)
    exp.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    success=Zen.Acquisition.Execute(exp)
    job.ClearTileRegionsAndPositions(0)
    return success



#runAcquisitionJobOffset(jobName='Marco-Dendra2-confocal', dx=0, dy=0, dz=0)






