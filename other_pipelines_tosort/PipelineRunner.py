﻿#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\Users\Tobias Kletter\auto-20190213' #has to be existing empty folder
nTimePointsImaging=300
timeIntervalImaging=600
timeIntervalPumping=1200
pumping=False

#advanced settings which user does not need to change
logFileName="experimentLog.txt"
pumpPortName='COM14'
defaultPositionsJobName='Default-Pipeline-Positions'

import sys
sys.path.append(r'D:\Users\FeedbackFiles--201902\zeiss-zenblue-automation\VisualStudio-tests')
from System.IO import Directory,Path,File
from datetime import datetime
import JobLogic as jl
import logutils
import pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()

if pumping:
    ser=SerialPort(pumpPortName,9600)
    #ser.Open()
    logutils.writeInfoLogMessage(messageText="Trying to connect to the pump: %s" %ser.IsOpen,logFilePath=logFileName)



#defining jobs
imagingJobList = list()
imagingJobList.append(jl.ImagingJob('leveling-reflection-40x-water','AF'))
imagingJobList.append(jl.ImagingJob('spindle-LZ','LowZoom'))
imagingJobList.append(jl.ImagingJob('HZ','HighZoom'))

#creating subfolders to save jobs data
for job in imagingJobList:
    Directory.CreateDirectory(job.getPipelineName())
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)
#job.ClearTileRegionsAndPositions(0)

def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    job
    return Zen.Acquisition.Execute(job)


def runAcquisitionTileJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddRectangleTileRegion(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,607,607,Zen.Devices.Focus.ActualPosition+dz)
    return Zen.Acquisition.Execute(job)
    
def getAutofocusOffset(image):
    image.Graphics.Clear()
    rectangle = image.Graphics.Add(ZenGraphicCategory.Rectangle)
    rectangle.SetBounds(0, 0,128, 1)

    table=Zen.Measurement.MeasureToTable(image)
    print table.RowCount


    maxValueIndex=0
    maxIntensity=0
    for index in range(1,table.RowCount):
        if table.GetValue(index,3)>maxIntensity:
            maxIntensity=table.GetValue(index,3)
            maxValueIndex=index

    sliceOffset=(table.RowCount-1)/2.0-maxValueIndex
    autofocusOffset=- sliceOffset*image.Metadata.ScalingMicron.Z
    #Zen.Application.Documents.Add(table)
    return autofocusOffset


previousPumptime=datetime(1970,1,1)
previousImagingTime=datetime(1970,1,1)
for timeIndex in range(0,nTimePointsImaging):
    while (datetime.now()-previousImagingTime).total_seconds()<timeIntervalImaging:
        print 'Time to wait %d s' %(timeIntervalImaging-(datetime.now()-previousImagingTime).total_seconds())
        time.sleep(1)
    previousImagingTime=datetime.now()
        
    for positionIndex in range(0,nPositions):
        p=positions[positionIndex]
        #acquire autofocus
        defaultExperimentName=imagingJobList[0].getZenName()
        Zen.Devices.Stage.MoveTo(p.X,p.Y)
        Zen.Devices.Focus.MoveTo(p.Z)
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[0].getPipelineName(),0,positionIndex+1,timeIndex+1)
        Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
        #Zen.Application.Documents.Add(acquiredImage)
        #autofocusOffset=getAutofocusOffset(acquiredImage)
        try:
            acquiredImage.Close()
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing file %s" %savePath,logFilePath=logFileName)
        
        fImage=Zen.Application.LoadImage(savePath)
        Zen.Application.Documents.Add(fImage)
        autofocusOffset=getAutofocusOffset(fImage)
        print 'AcquisitionOffset %f' %autofocusOffset
        logutils.writeInfoLogMessage(messageText="Autofocus offset %f" %autofocusOffset,logFilePath=logFileName)
        
        
        
        
        #acquire lowZooom Image
        defaultExperimentName=imagingJobList[1].getZenName()
        Zen.Devices.Stage.MoveTo(p.X,p.Y)
        Zen.Devices.Focus.MoveTo(p.Z)
        acquiredImage=runAcquisitionTileJobOffset(defaultExperimentName, 0, 0, autofocusOffset)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[1].getPipelineName(),0,positionIndex+1,timeIndex+1)
        Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)

        try:
            acquiredImage.Close()
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing file %s" %savePath,logFilePath=logFileName)

        try:
            fImage.Close()
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing autofocus file %s" %savePath,logFilePath=logFileName)
        
        
        
        if pumping and ((datetime.now()-previousPumptime).total_seconds()>timeIntervalPumping):
            sendPumpingSignal(ser)
            previousPumptime=datetime.now()
            logutils.writeInfoLogMessage(messageText="Send pumping signal",logFilePath=logFileName)
            time.sleep(10)
            


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
