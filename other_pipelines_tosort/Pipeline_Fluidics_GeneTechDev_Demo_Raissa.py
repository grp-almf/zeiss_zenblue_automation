﻿#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\USERS\Raissa\demo' #has to be existing folder
nTimePointsImaging=5 #has to be higher number than expected
fluidics=False

#advanced settings which user does not need to change
logFileName='experimentLog.txt'
defaultPositionsJobName='ASH-seqFISHplus-positions'
fluidicsPcIp='10.11.115.139' #IP-internet-adaptor-GenTechDev #'10.11.112.52' #'10.11.129.130'#IP-computer-GenTechDev
fluidicsPcPort=1234
useDefiniteFocus=False
useFeedbackMicroscopyFocus=True
updateZPositionFromAutofocus=True
timeDelayBeforeImaging=5



import socket
import subprocess
import json
import sys
from System.IO import Directory,Path,File
from datetime import datetime
import zeiss_zenblue_automation.utils.JobLogic as jl
import zeiss_zenblue_automation.utils.logutils as logutils
import zeiss_zenblue_automation.utils.pathutils as pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions

#defining jobs
#imagingJobs = {'FITC':             jl.ImagingJob('Microfluidics-40xW-FITC','FITC',7),
#               'Reflection':       jl.ImagingJob('Microfluidics-40xW-reflection','Reflection',-5),
#               }

imagingJobs = {'01-AF':                 jl.ImagingJob('AF-40x-water','AF',0,waitForFeedback=useFeedbackMicroscopyFocus),
               '02-DAPI':               jl.ImagingJob('raissa_demo','DAPI',5),
#               '03-readout':            jl.ImagingJob('ASH-seqFISHplus-readout','readout',9),
               }


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#connect to fluidics
if fluidics:
    microscopesocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    microscopesocket.connect((fluidicsPcIp,fluidicsPcPort))

#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()




#creating subfolders to save jobs data
for jkey in imagingJobs.keys():
    Directory.CreateDirectory(imagingJobs[jkey].getPipelineName())
#Directory.CreateDirectory(imagingJobs['LowZoom'].getPipelineName()+"_max")
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)


def runAcquisitionJobOffset(jobName, dx, dy, dz):
    exp = Zen.Acquisition.Experiments.GetByName(jobName)
    print 'job=%s dz=%f' %(jobName,dz)
    
    exp.SetActive()
    
    exp.ClearTileRegionsAndPositions(0)
    exp.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    #print zen.Devices.Focus.ActualPosition, dz
    success=Zen.Acquisition.Execute(exp)
    if success:
            logutils.writeInfoLogMessage(messageText="Acquired image %s at position X=%f; Y=%f; Z=%f" %(jobName,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz),logFilePath=logFileName)
    else:
            logutils.writeErrorLogMessage(messageText="Error generated by Zen when acquiring image %s at position X=%f; Y=%f; Z=%f" %(jobName,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz),logFilePath=logFileName)
    exp.ClearTileRegionsAndPositions(0)
    return success
    


def saveAndGetFeedback(image,path):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %path,logFilePath=logFileName)
    feedbackPath=pathutils.getDecisionPath(path)
    while not File.Exists(feedbackPath):
        print 'Waiting for decision file'
        time.sleep(1)
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        f.close()
        decisionDictionary=json.loads(decisionString)
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary
    

def getAutofocusOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            
            return (decisionDictionary['POSITIONS'].values()[0]['Z']-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0




def runDefaultPositionAcquisition(positionIndex, positionX,positionY,positionZ):
    
    try:
        Zen.Application.Documents.RemoveAll(False)
    except:
        logutils.writeWarningLogMessage(messageText="Problem with closing files",logFilePath=logFileName)
    
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    logutils.writeInfoLogMessage(messageText="Z Position before autofocus: %d" %Zen.Devices.Focus.ActualPosition,logFilePath=logFileName)
    
    time.sleep(timeDelayBeforeImaging)
    
    if useDefiniteFocus:
        Zen.Acquisition.FindSurface()
        logutils.writeInfoLogMessage(messageText="Z Position after hardware autofocus: %d" %Zen.Devices.Focus.ActualPosition,logFilePath=logFileName)
    
    print sorted(imagingJobs.keys())
    focusZOffset=0
    for imagingJobKey in sorted(imagingJobs.keys()):
        imagingJob=imagingJobs[imagingJobKey]
        defaultExperimentName=imagingJob.getZenName()
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, imagingJob.getZOffset()+focusZOffset)
        savePath=pathutils.generateSavePath(dataFolder,imagingJob.getPipelineName(),0,positionIndex+1,timeIndex+1)
        if imagingJob.getWaitForFeedback():
            focusZOffset+=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))
        else:
            Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Recorded image  %s" %savePath,logFilePath=logFileName)
    
    return focusZOffset



for timeIndex in range(0,nTimePointsImaging):
    try:
        
        print "Do it!"
        for positionIndex in range(0,nPositions):
            try:
                p=positions[positionIndex]
                focusZOffset=runDefaultPositionAcquisition(positionIndex=positionIndex, positionX=p.X,positionY=p.Y,positionZ=p.Z)
                if updateZPositionFromAutofocus:
                    p.Z=p.Z+focusZOffset
                    positions[positionIndex]=p #do we need this line or is it redundant?
            except Exception, e:
                logutils.writeErrorLogMessage(messageText="Unclassified error when running acquisition in one of default positions error message: %s" %(str(e)),logFilePath=logFileName)
        
        logutils.writeInfoLogMessage(messageText="Moving to the first position before launching fluidics",logFilePath=logFileName)
        Zen.Devices.Stage.MoveTo(positions[0].X,positions[0].Y)
        Zen.Devices.Focus.MoveTo(positions[0].Z)

        
        if fluidics:
            microscopesocket.sendall('signal')
            logutils.writeInfoLogMessage(messageText="Signal sent to fluidics",logFilePath=logFileName)
            print "Signal sent to fluidics"
            fluidicsMessage=microscopesocket.recv(20).decode('utf-8')
            
            if fluidicsMessage=='Done':
                logutils.writeInfoLogMessage(messageText="Fluidics sequence done",logFilePath=logFileName)
            else:
                logutils.writeErrorLogMessage(messageText="Unexpected response from fluidics",logFilePath=logFileName)
        pass
    except Exception , e: 
        logutils.writeErrorLogMessage(messageText="Unclassified error when running main loop. Error message: %s" %(str(e)),logFilePath=logFileName)
        exit(-1)


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
