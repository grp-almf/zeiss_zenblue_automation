﻿#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\USERS\Sanjana\test_auto' #has to be existing empty folder
nTimePointsImaging=1
timeIntervalImaging=20
timeIntervalPumping=1200
pumping=False

#advanced settings which user does not need to change
logFileName="experimentLog.txt"
pumpPortName='COM14'
defaultPositionsJobName='Default-Pipeline-Positions'


import os
import sys
import json
libraryPath=os.path.realpath(os.path.join(os.path.abspath( __file__ ),'../../utils'))

sys.path.append(libraryPath)

from System.IO import Directory,Path,File
from datetime import datetime
import JobLogic as jl
import logutils
import pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()

if pumping:
    ser=SerialPort(pumpPortName,9600)
    #ser.Open()
    logutils.writeInfoLogMessage(messageText="Trying to connect to the pump: %s" %ser.IsOpen,logFilePath=logFileName)



#defining jobs
imagingJobList = list()
imagingJobList.append(jl.ImagingJob('Sanjana_DAPI_AF','AF',0))
imagingJobList.append(jl.ImagingJob('Sanjana_DAPI_GFP_mcherry_confocal','LowZoom',0))

#creating subfolders to save jobs data
for job in imagingJobList:
    Directory.CreateDirectory(job.getPipelineName())
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)
#job.ClearTileRegionsAndPositions(0)


def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    #job
    return Zen.Acquisition.Execute(job)


def runAcquisitionTileJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddRectangleTileRegion(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,607,607,Zen.Devices.Focus.ActualPosition+dz)
    return Zen.Acquisition.Execute(job)
    

def saveAndGetFeedback(image,path):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %path,logFilePath=logFileName)
    feedbackPath=pathutils.getDecisionPath(path)
    while not File.Exists(feedbackPath):
        print 'Waiting for decision file'
        time.sleep(1)
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        print decisionString
        f.close()
        decisionDictionary=json.loads(decisionString)
        print decisionDictionary
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary


def getAutofocusOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            return (decisionDictionary['Z'][0]-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0

previousPumptime=datetime(1970,1,1)
previousImagingTime=datetime(1970,1,1)
for timeIndex in range(0,nTimePointsImaging):
    while (datetime.now()-previousImagingTime).total_seconds()<timeIntervalImaging:
        print 'Time to wait %d s' %(timeIntervalImaging-(datetime.now()-previousImagingTime).total_seconds())
        time.sleep(1)
    previousImagingTime=datetime.now()
        
    for positionIndex in range(0,nPositions):
        p=positions[positionIndex]
        positionX=p.X
        positionY=p.Y
        positionZ=p.Z
        
        #acquire autofocus
        defaultExperimentName=imagingJobList[0].getZenName()
        Zen.Devices.Stage.MoveTo(positionX,positionY)
        Zen.Devices.Focus.MoveTo(positionZ)
        
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[0].getPipelineName(),0,positionIndex+1,timeIndex+1)
        Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
        #Zen.Application.Documents.Add(acquiredImage)
        autofocusOffset=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))#getAutofocusOffset(acquiredImage)
        print autofocusOffset
        
        #acquire lowZooom Image
        defaultExperimentName=imagingJobList[1].getZenName()
        print defaultExperimentName
        Zen.Devices.Stage.MoveTo(positionX,positionY)
        Zen.Devices.Focus.MoveTo(positionZ)
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, autofocusOffset)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[1].getPipelineName(),0,positionIndex+1,timeIndex+1)
        logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
        Zen.Application.Save(acquiredImage,savePath)
        
        try:
            Zen.Application.Documents.RemoveAll(False)
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing files",logFilePath=logFileName)

      
        
        
        if pumping and ((datetime.now()-previousPumptime).total_seconds()>timeIntervalPumping):
            sendPumpingSignal(ser)
            previousPumptime=datetime.now()
            logutils.writeInfoLogMessage(messageText="Send pumping signal",logFilePath=logFileName)
            time.sleep(10)
            


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
