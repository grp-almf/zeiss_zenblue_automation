﻿#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\Users\Julian\auto_test' #has to be existing empty folder

#advanced settings which user does not need to change
logFileName="experimentLog.txt"
#defaultPositionsJobName='Default-Pipeline-Positions'
xCorr=0
yCorr=10

import sys
sys.path.append(r'D:\Users\FeedbackFiles--201902\VisualStudio-tests')
from System.IO import Directory,Path,File
from datetime import datetime
import JobLogic as jl
import logutils
import pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)



#defining jobs
autofocusJob=jl.ImagingJob('CLEM-AFocus-10x zoom3','AFocus10xDryZoom3',0)

imagingJobList = list()
imagingJobList.append(jl.ImagingJob('Julian 10x GFP zoom3','MZfluor',2))


#creating subfolders to save jobs data
Directory.CreateDirectory(autofocusJob.getPipelineName())
for job in imagingJobList:
    Directory.CreateDirectory(job.getPipelineName())
# Max projections for Low zoom image
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
#positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
#positions=positionsExperiment.GetSinglePositionInfos(0)
#job.ClearTileRegionsAndPositions(0)

#currentPosition={'X':Zen.Devices.Stage.ActualPositionX,'Y':Zen.Devices.Stage.ActualPositionY,'Z':Zen.Devices.Focus.ActualPosition}
positions=list()

activeImage=Zen.Application.ActiveDocument
for selection in activeImage.Graphics.GetEnumerator():
    xPixels=selection.Bounds.X+selection.Bounds.Width/2
    yPixels=selection.Bounds.Y+selection.Bounds.Height/2
    xStage=activeImage.Metadata.StagePositionMicron.X+(xPixels-(activeImage.Bounds.SizeX-1)/2.0)*activeImage.Scaling.X+xCorr
    yStage=activeImage.Metadata.StagePositionMicron.Y+(yPixels-(activeImage.Bounds.SizeY-1)/2.0)*activeImage.Scaling.Y+yCorr


    positions.append((xStage,yStage))


nPositions=len(positions)



print('Number of positions identified:  %d' %nPositions)


def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    job
    return Zen.Acquisition.Execute(job)


def getAutofocusOffset(image):
    image.Graphics.Clear()
    rectangle = image.Graphics.Add(ZenGraphicCategory.Rectangle)
    rectangle.SetBounds(0, 0,image.Bounds.SizeX, image.Bounds.SizeY)

    table=Zen.Measurement.MeasureToTable(image)
    print table.RowCount


    maxValueIndex=0
    maxIntensity=0
    for index in range(1,table.RowCount):
        if table.GetValue(index,3)>maxIntensity:
            maxIntensity=table.GetValue(index,3)
            maxValueIndex=index

    sliceOffset=(table.RowCount-1)/2.0-maxValueIndex
    autofocusOffset=- sliceOffset*image.Metadata.ScalingMicron.Z
    #Zen.Application.Documents.Add(table)
    return autofocusOffset


for positionIndex in range(0,nPositions):
    #Zen.Application.Documents.RemoveAll()


    acquiredImages=list()
    
    p=positions[positionIndex]
    #acquire autofocus
    defaultExperimentName=autofocusJob.getZenName()
    Zen.Devices.Stage.MoveTo(p[0],p[1])
    #Zen.Devices.Focus.MoveTo(p['Z'])
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
    savePath=pathutils.generateSavePath(dataFolder,autofocusJob.getPipelineName(),0,positionIndex+1,0)
    Zen.Application.Save(acquiredImage,savePath)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)

    try:
        acquiredImage.Close()
    except:
        logutils.writeWarningLogMessage(messageText="Problem with closing file %s" %savePath,logFilePath=logFileName)
    
    fImage=Zen.Application.LoadImage(savePath)
    Zen.Application.Documents.Add(fImage)
    autofocusOffset=getAutofocusOffset(fImage)
    print 'AcquisitionOffset %f' %autofocusOffset
    logutils.writeInfoLogMessage(messageText="Autofocus offset %f" %autofocusOffset,logFilePath=logFileName)
    acquiredImages.append(fImage)
    
    
    
    
    #acquire images
    for imagingJob in imagingJobList:
        defaultExperimentName=imagingJob.getZenName()
        Zen.Devices.Stage.MoveTo(p[0],p[1])
        #Zen.Devices.Focus.MoveTo(p['Z'])
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, autofocusOffset+imagingJob.getZOffset())
        savePath=pathutils.generateSavePath(dataFolder,imagingJob.getPipelineName(),0,positionIndex+1,0)
        logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
        acquiredImages.append(acquiredImage)
    


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
