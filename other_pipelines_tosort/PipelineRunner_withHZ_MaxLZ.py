﻿#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\Users\Tobias Kletter\auto-20190213' #has to be existing empty folder
nTimePointsImaging=300
timeIntervalImaging=600
timeIntervalPumping=1200
pumping=True

#advanced settings which user does not need to change
logFileName="experimentLog.txt"
pumpPortName='COM14'
defaultPositionsJobName='Default-Pipeline-Positions'

import sys
sys.path.append(r'D:\Users\FeedbackFiles--201902\zeiss-zenblue-automation\VisualStudio-tests')
from System.IO import Directory,Path,File
from datetime import datetime
import JobLogic as jl
import logutils
import pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()

if pumping:
    ser=SerialPort(pumpPortName,9600)
    #ser.Open()
    logutils.writeInfoLogMessage(messageText="Trying to connect to the pump: %s" %ser.IsOpen,logFilePath=logFileName)



#defining jobs
imagingJobList = list()
imagingJobList.append(jl.ImagingJob('AFocus-40x-water','AF'))
imagingJobList.append(jl.ImagingJob('spindle-LZ','LowZoom'))
imagingJobList.append(jl.ImagingJob('spindle-hz','HighZoom'))

#creating subfolders to save jobs data
for job in imagingJobList:
    Directory.CreateDirectory(job.getPipelineName())
# Max projections for Low zoom image
Directory.CreateDirectory(imagingJobList[1].getPipelineName()+"_max")
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)
#job.ClearTileRegionsAndPositions(0)

def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    job
    return Zen.Acquisition.Execute(job)


def runAcquisitionTileJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddRectangleTileRegion(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,607,607,Zen.Devices.Focus.ActualPosition+dz)
    return Zen.Acquisition.Execute(job)
    
def getAutofocusOffset(image):
    image.Graphics.Clear()
    rectangle = image.Graphics.Add(ZenGraphicCategory.Rectangle)
    rectangle.SetBounds(0, 0,128, 1)

    table=Zen.Measurement.MeasureToTable(image)
    print table.RowCount


    maxValueIndex=0
    maxIntensity=0
    for index in range(1,table.RowCount):
        if table.GetValue(index,3)>maxIntensity:
            maxIntensity=table.GetValue(index,3)
            maxValueIndex=index

    sliceOffset=(table.RowCount-1)/2.0-maxValueIndex
    autofocusOffset=- sliceOffset*image.Metadata.ScalingMicron.Z
    #Zen.Application.Documents.Add(table)
    return autofocusOffset

def getTargetCoordinates(lowZoomImage):

    markerSize=40
    image2 = Zen.Processing.Filter.Smooth.Median(lowZoomImage, 3, 3, 1, ZenThirdProcessingDimension.None, False)

    imageMax = Zen.Processing.Transformation.Geometric.OrthoView(image2, ZenViewDirection.Frontal, 0, image2.Bounds.SizeZ, ZenProjectionMethod.Maximum, False, False)
    image2.Close()
    Zen.Application.Documents.Add(imageMax)

    analysissetting1 = ZenImageAnalysisSetting()
    analysissetting1.Load("Metap_02")
    table1=Zen.Analyzing.AnalyzeToTable(imageMax,analysissetting1)[0]
    Zen.Application.Documents.Add(table1)

    logutils.writeInfoLogMessage(messageText="Found %d target positions" %table1.RowCount,logFilePath=logFileName)
    
    targetCoordinates=list()
    
    for selectionIndex in range(0,table1.RowCount):
        logutils.writeInfoLogMessage(messageText="Target position %d: %f %f" %(selectionIndex,table1.GetValue(selectionIndex,7),table1.GetValue(selectionIndex,8)),logFilePath=logFileName)
        circle1 = imageMax.Graphics.Add(ZenGraphicCategory.Circle)
        circle1.SetBounds(table1.GetValue(selectionIndex,7)-markerSize/2, table1.GetValue(selectionIndex,8)-markerSize/2, markerSize, markerSize)
        circle1.Thickness = 1
        circle1.StrokeColor = ZenColors.White
        circle1.TextColor = ZenColors.White
        circle1.IsMeasurementVisible = False    
        targetCoordinates.append((table1.GetValue(selectionIndex,2),table1.GetValue(selectionIndex,3)))
    
    return imageMax,table1,targetCoordinates


previousPumptime=datetime(1970,1,1)
previousImagingTime=datetime(1970,1,1)
for timeIndex in range(0,nTimePointsImaging):
    while (datetime.now()-previousImagingTime).total_seconds()<timeIntervalImaging:
        print 'Time to wait %d s' %(timeIntervalImaging-(datetime.now()-previousImagingTime).total_seconds())
        time.sleep(1)
    previousImagingTime=datetime.now()
        
    for positionIndex in range(0,nPositions):
        acquiredImages=list()
        
        p=positions[positionIndex]
        #acquire autofocus
        defaultExperimentName=imagingJobList[0].getZenName()
        Zen.Devices.Stage.MoveTo(p.X,p.Y)
        Zen.Devices.Focus.MoveTo(p.Z)
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[0].getPipelineName(),0,positionIndex+1,timeIndex+1)
        #while True:
        #    disposed=acquiredImage.IsDisposed
        #    logutils.writeInfoLogMessage(messageText="Autofocus image %s is disposed: %s" %(savePath,disposed),logFilePath=logFileName)
        #    if disposed:
        #        break
        #    else:
        #        time.sleep(1)
        Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
        #Zen.Application.Documents.Add(acquiredImage)
        #autofocusOffset=getAutofocusOffset(acquiredImage)
        try:
            acquiredImage.Close()
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing file %s" %savePath,logFilePath=logFileName)
        
        fImage=Zen.Application.LoadImage(savePath)
        Zen.Application.Documents.Add(fImage)
        autofocusOffset=getAutofocusOffset(fImage)
        print 'AcquisitionOffset %f' %autofocusOffset
        logutils.writeInfoLogMessage(messageText="Autofocus offset %f" %autofocusOffset,logFilePath=logFileName)
        acquiredImages.append(fImage)
        
        
        
        
        #acquire lowZooom Image
        defaultExperimentName=imagingJobList[1].getZenName()
        Zen.Devices.Stage.MoveTo(p.X,p.Y)
        Zen.Devices.Focus.MoveTo(p.Z)
        acquiredImage=runAcquisitionTileJobOffset(defaultExperimentName, 0, 0, autofocusOffset)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[1].getPipelineName(),0,positionIndex+1,timeIndex+1)
        logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
        acquiredImages.append(acquiredImage)
        
        #process LowZoom Image
        imageMax,resultsTable,selectedPositions=getTargetCoordinates(lowZoomImage=acquiredImage)
        acquiredImages.append(imageMax)
        acquiredImages.append(resultsTable)
        
        Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Saved image %s" %savePath,logFilePath=logFileName)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[1].getPipelineName()+"_max",0,positionIndex+1,timeIndex+1)
        Zen.Application.Save(imageMax,savePath)
        logutils.writeInfoLogMessage(messageText="Saved image %s" %savePath,logFilePath=logFileName)


        
        #acquire HighZoom images
        defaultExperimentName=imagingJobList[2].getZenName()
        for selectedPosition in selectedPositions:
            Zen.Devices.Stage.MoveTo(selectedPosition[0],selectedPosition[1])
            acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, autofocusOffset)
            savePath=pathutils.generateSavePath(dataFolder,imagingJobList[2].getPipelineName(),0,positionIndex+1,timeIndex+1)
            Zen.Application.Save(acquiredImage,savePath)
            logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
            acquiredImages.append(acquiredImage)

        try:
            acquiredImage.Close()
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing file %s" %savePath,logFilePath=logFileName)

        try:
            fImage.Close()
        except:
            logutils.writeWarningLogMessage(messageText="Problem with closing autofocus file %s" %savePath,logFilePath=logFileName)
        
        
        
        if pumping and ((datetime.now()-previousPumptime).total_seconds()>timeIntervalPumping):
            sendPumpingSignal(ser)
            previousPumptime=datetime.now()
            logutils.writeInfoLogMessage(messageText="Send pumping signal",logFilePath=logFileName)
            time.sleep(10)
            
        for image in acquiredImages:
            try:
                image.Close()
            except:
                logutils.writeWarningLogMessage(messageText="Problem with closing file",logFilePath=logFileName)

            


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
