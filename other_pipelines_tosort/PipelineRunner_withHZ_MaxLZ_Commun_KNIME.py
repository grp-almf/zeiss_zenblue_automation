﻿%reset #reset all global variables
#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\Users\Tobias_Kletter\auto--20190823' #has to be existing empty folder
knimeWorkflowPath= r'D:\Users\FeedbackFiles--201902\KNIME_WORKSPACE\spindle-feedback-kletter-knime\Image-Machine-Learning-Spindle_online'
prefPath=r'D:\Users\FeedbackFiles--201902\KNIME_WORKSPACE\knime_prefs-20190819.epf'
nTimePointsImaging=600
timeIntervalImaging=600
timeIntervalPumping=1200
pumping=True

#advanced settings which user does not need to change
logFileName="experimentLog.txt"
pumpPortName='COM15'
defaultPositionsJobName='Default-Pipeline-Positions'
markerSize=10

import subprocess
import json
import sys
sys.path.append(r'D:\Users\FeedbackFiles--201902\zeiss-zenblue-automation\VisualStudio-tests')
from System.IO import Directory,Path,File
from datetime import datetime
import JobLogic as jl
import logutils
import pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()

if pumping:
    ser=SerialPort(pumpPortName,9600)
    #ser.Open()
    logutils.writeInfoLogMessage(messageText="Trying to connect to the pump: %s" %ser.IsOpen,logFilePath=logFileName)



#defining jobs
imagingJobList = list()
imagingJobList.append(jl.ImagingJob('AFocus-40x-water','AF',0))
imagingJobList.append(jl.ImagingJob('spindle-LZ','LowZoom',0))
imagingJobList.append(jl.ImagingJob('AFocus-40x-water-hz','AFHighZoom',0))
imagingJobList.append(jl.ImagingJob('spindle-hz','HighZoom',0))

#creating subfolders to save jobs data
for job in imagingJobList:
    Directory.CreateDirectory(job.getPipelineName())
Directory.CreateDirectory(imagingJobList[1].getPipelineName()+"_max")
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)

def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    success=Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success


def runAcquisitionTileJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddRectangleTileRegion(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,607,607,Zen.Devices.Focus.ActualPosition+dz)
    success = Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success
    
def getAutofocusOffset(image):
    image.Graphics.Clear()
    rectangle = image.Graphics.Add(ZenGraphicCategory.Rectangle)
    rectangle.SetBounds(0, 0,128, 1)

    table=Zen.Measurement.MeasureToTable(image)
    print table.RowCount


    maxValueIndex=0
    maxIntensity=0
    for index in range(1,table.RowCount):
        if table.GetValue(index,3)>maxIntensity:
            maxIntensity=table.GetValue(index,3)
            maxValueIndex=index

    sliceOffset=(table.RowCount-1)/2.0-maxValueIndex
    autofocusOffset=- sliceOffset*image.Metadata.ScalingMicron.Z
    #Zen.Application.Documents.Add(table)
    return autofocusOffset



def saveAndGetFeedback(image,path):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %path,logFilePath=logFileName)
    feedbackPath=pathutils.getDecisionPath(path)
    while not File.Exists(feedbackPath):
        print 'Waiting for decision file'
        time.sleep(1)
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        f.close()
        decisionDictionary=json.loads(decisionString)
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary
    
def callKnimeWorkflow(workflowPath,imageFilePath):
    
    cmdOptions = 'knime -consoleLog -nosplash -reset -application org.knime.product.KNIME_BATCH_APPLICATION -preferences=%s -workflowDir=%s -nosave -workflow.variable=imagepath,%s,String' %(prefPath,workflowPath,imageFilePath)
    sret = subprocess.check_call(cmdOptions)
    
    return sret

    
def saveAndGetFeedbackKnime(image,path):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %path,logFilePath=logFileName)
    
    knimeSuccess=callKnimeWorkflow(knimeWorkflowPath,path)
    if knimeSuccess is not 0:
        logutils.writeErrorLogMessage(messageText="Error when processing file %s in Knime. Error code %d" %(path,knimeSuccess),logFilePath=logFileName)
    
    logutils.writeInfoLogMessage(messageText="Successfully processed image file %s in Knime" %path,logFilePath=logFileName)
    
    feedbackPath=pathutils.getDecisionPath(path)
    while not File.Exists(feedbackPath):
        print 'Waiting for decision file'
        time.sleep(1)
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        f.close()
        decisionDictionary=json.loads(decisionString)
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary

def getAutofocusOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            return (decisionDictionary['Z'][0]-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0
        
def getHZPositions(image,decisionDictionary):
    xValues=decisionDictionary['X']
    yValues=decisionDictionary['Y']
    
    positionsPixels=list()
    positionsStage=list()
    for i in range(len(xValues)):
        positionsPixels.append((xValues[i],yValues[i]))
        xStage=(xValues[i]-(image.Bounds.SizeX-1)/2.0)*image.Scaling.X
        yStage=(yValues[i]-(image.Bounds.SizeY-1)/2.0)*image.Scaling.Y
        positionsStage.append((xStage,yStage))
    return positionsStage,positionsPixels
    
def markSelectedPositions(image,positions):


    for position in positions:
        circle1 = image.Graphics.Add(ZenGraphicCategory.Circle)
        circle1.SetBounds(position[0]-markerSize/2, position[1]-markerSize/2, markerSize, markerSize)
        circle1.Thickness = 1
        circle1.StrokeColor = ZenColors.White
        circle1.IsMeasurementVisible = False    

    return


def runDefaultPositionAcquisition(positionIndex, positionX,positionY,positionZ):

    acquiredImages=list()
    
    #acquire autofocus
    defaultExperimentName=imagingJobList[0].getZenName()
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobList[0].getPipelineName(),0,positionIndex+1,timeIndex+1)
    Zen.Application.Save(acquiredImage,savePath)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
    #Zen.Application.Documents.Add(acquiredImage)
    autofocusOffset=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))#getAutofocusOffset(acquiredImage)
    acquiredImages.append(acquiredImage)
    print autofocusOffset
    
    #acquire lowZooom Image
    defaultExperimentName=imagingJobList[1].getZenName()
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    acquiredImage=runAcquisitionTileJobOffset(defaultExperimentName, 0, 0, autofocusOffset)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobList[1].getPipelineName(),0,positionIndex+1,timeIndex+1)
    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    acquiredImages.append(acquiredImage)
    
    #process LowZoom Image
    selectedPositionsStage,selectedPositionsPixels=getHZPositions(acquiredImage,saveAndGetFeedbackKnime(acquiredImage,savePath))
    
    markSelectedPositions(acquiredImage,selectedPositionsPixels)
    
    Zen.Application.Save(acquiredImage,savePath)
    logutils.writeInfoLogMessage(messageText="Saved image %s" %savePath,logFilePath=logFileName)

    
    #acquire HighZoom images
    for selectedPosition in selectedPositionsStage:

        Zen.Devices.Stage.MoveTo(positionX,positionY)
        Zen.Devices.Focus.MoveTo(positionZ+autofocusOffset)
        defaultExperimentName=imagingJobList[2].getZenName()
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, selectedPosition[0], selectedPosition[1], 0)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[2].getPipelineName(),0,positionIndex+1,timeIndex+1)
        Zen.Application.Save(acquiredImage,savePath)
        #logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
        acquiredImages.append(acquiredImage)
        #acquiredImage.Close()
        
        hzAutofocusOffset=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))#getAutofocusOffset(acquiredImage)

        Zen.Devices.Stage.MoveTo(positionX,positionY)
        Zen.Devices.Focus.MoveTo(positionZ+autofocusOffset)
        defaultExperimentName=imagingJobList[3].getZenName()
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, selectedPosition[0],selectedPosition[1], hzAutofocusOffset)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobList[3].getPipelineName(),0,positionIndex+1,timeIndex+1)
        Zen.Application.Save(acquiredImage,savePath)
        logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %savePath,logFilePath=logFileName)
        acquiredImages.append(acquiredImage)
        #acquiredImage.Close()


    try:
        Zen.Application.Documents.RemoveAll(False)
    except:
        logutils.writeWarningLogMessage(messageText="Problem with closing files",logFilePath=logFileName)


    #for image in acquiredImages:
    #    try:
    #        image.Close()
    #    except:
    #        logutils.writeWarningLogMessage(messageText="Problem with closing file",logFilePath=logFileName)
    
    pass
    return



previousPumptime=datetime(1970,1,1)
previousImagingTime=datetime(1970,1,1)
print "Now %s" %datetime.now()
print "Previous  %s" %previousImagingTime
for timeIndex in range(0,nTimePointsImaging):
    try:
        while (datetime.now()-previousImagingTime).total_seconds()<timeIntervalImaging:
            print 'Time to wait %d s' %(timeIntervalImaging-(datetime.now()-previousImagingTime).total_seconds())
            time.sleep(1)
        previousImagingTime=datetime.now()
        
        
        print "Do it!"
        for positionIndex in range(0,nPositions):
            try:
                p=positions[positionIndex]
                runDefaultPositionAcquisition(positionIndex=positionIndex, positionX=p.X,positionY=p.Y,positionZ=p.Z)
            except Exception, e:
                logutils.writeErrorLogMessage(messageText="Unclassified error when running acquisition in one of default positions error message: %s" %(str(e)),logFilePath=logFileName)
            
            if pumping and ((datetime.now()-previousPumptime).total_seconds()>timeIntervalPumping):
                sendPumpingSignal(ser)
                previousPumptime=datetime.now()
                logutils.writeInfoLogMessage(messageText="Send pumping signal",logFilePath=logFileName)
                time.sleep(10)
    except Exception , e: 
        logutils.writeErrorLogMessage(messageText="Unclassified error when running main loop. Error message: %s" %(str(e)),logFilePath=logFileName)


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
