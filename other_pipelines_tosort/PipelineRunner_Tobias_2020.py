﻿#%reset #reset all global variables
#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\USERS\Tobias\20200807' #has to be existing empty folder
nTimePointsImaging=600
timeIntervalImaging=600
timeIntervalPumping=1200
maxFeedbackTime=600
pumping=True
identifyPositionsAndGetHZ=True
desiredWidthLZ=319#607
desiredHeightLZ=319#607
doStitching=False

#advanced settings which user does not need to change
stitchingSettingsName='TobiasStitch'
stitchingFileTag='LowZoomStitch'
logFileName="experimentLog.txt"
pumpPortName='COM3'
defaultPositionsJobName='Default-Pipeline-Positions-Tobias'
markerSize=10

import json
import sys
from System.IO import Directory,Path,File
from datetime import datetime
import zeiss_zenblue_automation.utils.JobLogic as jl
import zeiss_zenblue_automation.utils.logutils as logutils
import zeiss_zenblue_automation.utils.pathutils as pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions

#defining jobs
imagingJobs = {'AF':                jl.ImagingJob('Tobias-AF','AF',0),
               'LowZoom':           jl.ImagingJob('Tobias-LZ-confocal','LowZoom',5),
               'AFHighZoom':        jl.ImagingJob('Tobias-AFhighzoom','AFHighZoom',0),
               'HighZoom':          jl.ImagingJob('Tobias-HighZoom','HighZoom',0)
              }



#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#creating subfolders to save jobs data
for ikey in imagingJobs.keys():
    Directory.CreateDirectory(imagingJobs[ikey].getPipelineName())
Directory.CreateDirectory(stitchingFileTag)
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)


#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()

if pumping:
    ser=SerialPort(pumpPortName,9600)
    #ser.Open()
    #logutils.writeInfoLogMessage(messageText="Trying to connect to the pump: %s" %ser.IsOpen,logFilePath=logFileName)






#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)


def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    success=Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success


def runAcquisitionTileJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddRectangleTileRegion(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,desiredWidthLZ,desiredHeightLZ,Zen.Devices.Focus.ActualPosition+dz)
    success = Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success
    


def saveAndGetFeedback(image,path,getFeedback=True):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Saved image %s" %path,logFilePath=logFileName)
    if not getFeedback:
        return None
    feedbackPath=pathutils.getDecisionPath(path)
    feedbackTime=0
    while ((not File.Exists(feedbackPath)) and (feedbackTime<maxFeedbackTime)):
        print 'Waiting for decision file'
        time.sleep(1)
        feedbackTime=feedbackTime+1
    
    if (feedbackTime>=maxFeedbackTime):
        logutils.writeErrorLogMessage(messageText="Reached maximum feedback time when waiting for the feedback of file %s" %path,logFilePath=logFileName)
        return None
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        f.close()
        decisionDictionary=json.loads(decisionString)
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary
    

def getAutofocusOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            
            return (decisionDictionary['POSITIONS'].values()[0]['Z']-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0


def get3DOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            posDictionary=decisionDictionary['POSITIONS'].values()[0]
            xOffset=(posDictionary['X']-(image.Bounds.SizeX-1)/2.0)*image.Scaling.X
            yOffset=(posDictionary['Y']-(image.Bounds.SizeY-1)/2.0)*image.Scaling.Y
            zOffset=(posDictionary['Z']-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
            return xOffset,yOffset,zOffset
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0,0,0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0,0,0

    
def getHZPositions(image,decisionDictionary):
    positionsDictionary=decisionDictionary['POSITIONS']
    
    positionsPixels=list()
    positionsStage=list()
    for k in sorted(positionsDictionary.keys()):
        pos=positionsDictionary[k]
        positionsPixels.append((pos['X'],pos['Y']))
        xStage=(pos['X']-(image.Bounds.SizeX-1)/2.0)*image.Scaling.X
        yStage=(pos['Y']-(image.Bounds.SizeY-1)/2.0)*image.Scaling.Y
        positionsStage.append((xStage,yStage))
    return positionsStage,positionsPixels
    
    

def runDefaultPositionAcquisition(positionIndex, positionX,positionY,positionZ):

    acquiredImages=list()
    
    #acquire autofocus
    defaultExperimentName=imagingJobs['AF'].getZenName()
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
    acquiredImages.append(acquiredImage)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['AF'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    autofocusOffset=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))#getAutofocusOffset(acquiredImage)
    print autofocusOffset
    
    #acquire lowZooom Image
    defaultExperimentName=imagingJobs['LowZoom'].getZenName()
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    acquiredImage=runAcquisitionTileJobOffset(defaultExperimentName, 0, 0, imagingJobs['LowZoom'].getZOffset()+autofocusOffset)
    acquiredImages.append(acquiredImage)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['LowZoom'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    saveAndGetFeedback(acquiredImage,savePath,getFeedback=False)
    
    
    stitchSavePath=pathutils.generateSavePath(dataFolder,stitchingFileTag,0,positionIndex+1,timeIndex+1)
    if doStitching:
        stitchImage=Zen.Processing.Transformation.Geometric.Stitching(acquiredImage,None,Zen.Processing.Transformation.Settings.StitchingSetting(stitchingSettingsName))
    else:
        stitchImage=acquiredImage.Clone()
    acquiredImages.append(stitchImage)
    logutils.writeInfoLogMessage(messageText="Generated stitch image %s" %stitchSavePath,logFilePath=logFileName)
    Zen.Application.Documents.Add(stitchImage)
    lzFeedback=saveAndGetFeedback(stitchImage,stitchSavePath,getFeedback=identifyPositionsAndGetHZ)

    if not identifyPositionsAndGetHZ:
        return acquiredImages
    
    selectedPositionsStage,selectedPositionsPixels=getHZPositions(stitchImage,lzFeedback)
    
    
    ## acquire HighZoom images
    for selectedPosition in selectedPositionsStage:
        Zen.Devices.Stage.MoveTo(positionX,positionY)
        Zen.Devices.Focus.MoveTo(positionZ+autofocusOffset)
        defaultExperimentName=imagingJobs['AFHighZoom'].getZenName()
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, selectedPosition[0], selectedPosition[1], 0)
        acquiredImages.append(acquiredImage)
        logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
        
        savePath=pathutils.generateSavePath(dataFolder,imagingJobs['AFHighZoom'].getPipelineName(),0,positionIndex+1,timeIndex+1)
        
        hzOffsetX,hzOffsetY,hzOffsetZ=get3DOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))#getAutofocusOffset(acquiredImage)
        
        Zen.Devices.Stage.MoveTo(positionX,positionY)
        Zen.Devices.Focus.MoveTo(positionZ+autofocusOffset)
        defaultExperimentName=imagingJobs['HighZoom'].getZenName()
        acquiredImage=runAcquisitionJobOffset(defaultExperimentName, selectedPosition[0]+hzOffsetX,selectedPosition[1]+hzOffsetY, hzOffsetZ)
        acquiredImages.append(acquiredImage)
        savePath=pathutils.generateSavePath(dataFolder,imagingJobs['HighZoom'].getPipelineName(),0,positionIndex+1,timeIndex+1)
        logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
        saveAndGetFeedback(acquiredImage,savePath,getFeedback=False)
    
    pass
    return acquiredImages



previousPumptime=datetime(1970,1,1)
previousImagingTime=datetime(1970,1,1)
print "Now %s" %datetime.now()
print "Previous  %s" %previousImagingTime
for timeIndex in range(0,nTimePointsImaging):
    try:
        while (datetime.now()-previousImagingTime).total_seconds()<timeIntervalImaging:
            print 'Time to wait %d s' %(timeIntervalImaging-(datetime.now()-previousImagingTime).total_seconds())
            time.sleep(1)
        previousImagingTime=datetime.now()
        
        
        print "Do it!"
        for positionIndex in range(0,nPositions):
            try:
                p=positions[positionIndex]
                acquiredImages=runDefaultPositionAcquisition(positionIndex=positionIndex, positionX=p.X,positionY=p.Y,positionZ=p.Z)
                try:
                    time.sleep(5)
                    logutils.writeInfoLogMessage(messageText="Trying to close current images",logFilePath=logFileName)
                    Zen.Application.Documents.RemoveAll(False)
                    logutils.writeInfoLogMessage(messageText="All images were removed from the GUI",logFilePath=logFileName)
                    for acquiredImage in acquiredImages:
                        acquiredImage.Close()
                    logutils.writeInfoLogMessage(messageText="All images were closed",logFilePath=logFileName)
                except:
                    logutils.writeWarningLogMessage(messageText="Problem with closing files",logFilePath=logFileName)
            except Exception, e:
                logutils.writeErrorLogMessage(messageText="Unclassified error when running acquisition in one of default positions error message: %s" %(str(e)),logFilePath=logFileName)
            logutils.writeInfoLogMessage(messageText="Checking if pumping is required",logFilePath=logFileName)
            if pumping and ((datetime.now()-previousPumptime).total_seconds()>timeIntervalPumping):
                logutils.writeInfoLogMessage(messageText="Trying to send pumping signal",logFilePath=logFileName)
                sendPumpingSignal(ser)
                previousPumptime=datetime.now()
                logutils.writeInfoLogMessage(messageText="Pumping signal sent",logFilePath=logFileName)
                time.sleep(10)
    except Exception , e: 
        logutils.writeErrorLogMessage(messageText="Unclassified error when running main loop. Error message: %s" %(str(e)),logFilePath=logFileName)


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
