﻿#%reset #reset all global variables
#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\USERS\ALMF\Manuel\ArgoLight\ImageData' #has to be existing empty folder
maxFeedbackTime=600

startX = Zen.Devices.Stage.ActualPositionX
startY = Zen.Devices.Stage.ActualPositionY
startZ = Zen.Devices.Focus.ActualPosition

stepCoarse = 5
stepFine = 0.5


#advanced settings which user does not need to change

logFileName="experimentLog.txt"
defaultPositionsJobName='Default-Pipeline-Positions-Tobias'
acquiredImages=list()

#initialize ArgoLight positions. Start is center of field of rings:

currentZ = 0

import json
import sys
from System.IO import Directory,Path,File
from datetime import datetime
import zeiss_zenblue_automation.utils.JobLogic as jl
import zeiss_zenblue_automation.utils.logutils as logutils
import zeiss_zenblue_automation.utils.pathutils as pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions

#defining jobs
imagingJobs = {'AFcoarse':                  jl.ImagingJob('AF_ArgoLight_coarse', 'AFcoarse', 0),
               'AFfine':                    jl.ImagingJob('AF_ArgoLight_fine', 'AFfine', 0),
               'FieldOfRings':              jl.ImagingJob('Argolight_fieldofrings_tile', 'FieldOfRings', 0),
               '2x16ItensityGradation':     jl.ImagingJob('Argolight_2x16_tile','2x16ItensityGradation', 0),
               'GradualSpacedLines_hor':    jl.ImagingJob('Argolight_resolution_hor','GradualSpacedLines_hor',0),
               'GradualSpacedLines_ver':    jl.ImagingJob('Argolight_resolution_ver','GradualSpacedLines_ver',0)
              }



#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#creating subfolders to save jobs data
for ikey in imagingJobs.keys():
    Directory.CreateDirectory(imagingJobs[ikey].getPipelineName())
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)

    
def runAcquisitionJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    print Zen.Devices.Focus.ActualPosition, dz
    success=Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success


def saveAndGetFeedback(image,path,getFeedback=True):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Saved image %s" %path,logFilePath=logFileName)
    if not getFeedback:
        return None
    feedbackPath=pathutils.getDecisionPath(path)
    feedbackTime=0
    while ((not File.Exists(feedbackPath)) and (feedbackTime<maxFeedbackTime)):
        print 'Waiting for decision file'
        time.sleep(1)
        feedbackTime=feedbackTime+1
        
    if (feedbackTime>=maxFeedbackTime):
        logutils.writeErrorLogMessage(messageText="Reached maximum feedback time when waiting for the feedback of file %s" %path,logFilePath=logFileName)
        return None
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        f.close()
        decisionDictionary=json.loads(decisionString)
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary
    

def getAutofocusOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            return (decisionDictionary['POSITIONS'].values()[0]['Z']-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0


def get3DOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            posDictionary=decisionDictionary['POSITIONS'].values()[0]
            xOffset=(posDictionary['X']-(image.Bounds.SizeX-1)/2.0)*image.Scaling.X
            yOffset=(posDictionary['Y']-(image.Bounds.SizeY-1)/2.0)*image.Scaling.Y
            zOffset=(posDictionary['Z']-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
            return xOffset,yOffset,zOffset
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0,0,0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0,0,0


def runAcquisitionSequence(posXoffset, posYoffset, jobName):
    Zen.Devices.Stage.MoveTo(startX + posXoffset,startY + posYoffset)
    
    #acquire autofocus coarse
    defaultExperimentName=imagingJobs['AFcoarse'].getZenName()    
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
    acquiredImages.append(acquiredImage)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['AFcoarse'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    autofocusOffsetC=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))

    
    #acquire autofocus fine
    defaultExperimentName=imagingJobs['AFfine'].getZenName()    
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0+autofocusOffsetC)
    acquiredImages.append(acquiredImage)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['AFfine'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    autofocusOffsetF=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))

    #----------------------------------------
    # insert potential template matching here
    #----------------------------------------
    
    #acquire jobName Image
    defaultExperimentName=imagingJobs[jobName].getZenName()
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, imagingJobs[jobName].getZOffset()+autofocusOffsetC+autofocusOffsetF)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs[jobName].getPipelineName(),0,positionIndex+1,timeIndex+1)
    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    saveAndGetFeedback(acquiredImage,savePath,getFeedback=False)
    
    try:
        time.sleep(5)
        logutils.writeInfoLogMessage(messageText="Trying to close current images",logFilePath=logFileName)
        Zen.Application.Documents.RemoveAll(False)
        logutils.writeInfoLogMessage(messageText="All images were removed from the GUI",logFilePath=logFileName)
        for acquiredImage in acquiredImages:
            acquiredImage.Close()
        logutils.writeInfoLogMessage(messageText="All images were closed",logFilePath=logFileName)
    except:
        logutils.writeWarningLogMessage(messageText="Problem with closing files",logFilePath=logFileName)
    
    return 0
    

try:
    print "Do it!"
    try:
        # ToDo: implement success control here; for instance watch the autofocus over
        # a period of time and only start field of rings when the difference of two 
        # subsequent runs (e.g. 5 min apart) is below a critical threshold.
        
        # ToDo: Also when the quality control is implemented automatically, a 
        # re-execution can be triggered if the result is not within the expected 
        # parameters - up to a limit of maybe three reexecutions.
        
        # ToDo: The individual acquisition sequences could be specified in a list which 
        # is iterated over here. Each acquisition sequence could have it's own set of 
        # parameters, for instance for the request of feedback or the quality control 
        # analysis and the number of reruns.
        positionIndex = 0
        timeIndex = 0
        
        startX = Zen.Devices.Stage.ActualPositionX
        startY = Zen.Devices.Stage.ActualPositionY
        startZ = Zen.Devices.Focus.ActualPosition
        for irep in range(0, 1):
            runAcquisitionSequence(0, 0, 'FieldOfRings')
            runAcquisitionSequence(750, 700, '2x16ItensityGradation')
            runAcquisitionSequence(750, 1000, 'GradualSpacedLines_hor')
            runAcquisitionSequence(1250, 1000, 'GradualSpacedLines_ver')
        
        Zen.Devices.Stage.MoveTo(startX,startY)
    except Exception, e:
        logutils.writeErrorLogMessage(messageText="Unclassified error when running acquisition in one of default positions error message: %s" %(str(e)),logFilePath=logFileName)
except Exception , e: 
    logutils.writeErrorLogMessage(messageText="Unclassified error when running main loop. Error message: %s" %(str(e)),logFilePath=logFileName)
    

print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)

