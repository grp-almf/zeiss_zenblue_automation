# Activities


IronPython Scripts for running automated experiments with OAD in ZenBlue


## 2019-11-12

- Merging in old repository **zenblue-automation**. Old repository deleted from gitlab.

## 2019-07-23

- Backing up script files from Zeiss LSM800
