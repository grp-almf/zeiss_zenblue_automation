﻿from System.IO import Path, File
from xml.etree import ElementTree

def generateSaveName(jobName, well, position, time, repeat):
    w0=0
    if type(well)==type(w0):
        if repeat == 0:
            return '%s--W%04d--P%04d-T%04d.czi' %(jobName,well,position,time)
        else:
            return '%s--W%04d--P%04d-T%04d--%04d.czi' %(jobName,well,position,time,repeat)
    else:
        if repeat == 0:
            return '%s--W%s--P%04d-T%04d.czi' %(jobName,well,position,time)
        else:
            return '%s--W%s--P%04d-T%04d--%04d.czi' %(jobName,well,position,time,repeat)

def generateSavePath(rootFolder, jobName, well, position, time):
    saveFolder=Path.Combine(rootFolder,jobName)

    repeat=0
    while True:
        filename=generateSaveName(jobName, well, position, time, repeat)
        savePath=Path.Combine(saveFolder,filename)
        
        if File.Exists(savePath):
            repeat=repeat+1
        else:
            break
        
    return savePath


def getDecisionPath(sourceImagePath):
    return Path.Combine(Path.GetDirectoryName(sourceImagePath), (Path.GetFileNameWithoutExtension(sourceImagePath)+'_feedback.json'))
