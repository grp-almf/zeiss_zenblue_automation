from System.IO import File
from datetime import datetime
from System.Text import UTF8Encoding

def writeInfoLogMessage(messageText,logFilePath):
    writeLogMessage('[INFO]:',messageText,logFilePath)

def writeErrorLogMessage(messageText,logFilePath):
    writeLogMessage('[ERROR]:',messageText,logFilePath)

def writeWarningLogMessage(messageText,logFilePath):
    writeLogMessage('[WARNING]:',messageText,logFilePath)


def writeLogMessage(messageType,messageText,logFilePath):
    
    logStreamWriter= getStreamWriter(logFilePath)
    logStreamWriter.WriteLine('%s\t%s\t%s' %(messageType,datetime.now().strftime('%Y/%m/%d %H:%M:%S'),messageText))
    
    logStreamWriter.Close()


def getStreamWriter(logFilePath):
    if File.Exists(logFilePath):
        return File.AppendText(logFilePath)
    
    logStreamWriter=File.AppendText(logFilePath)
    logStreamWriter.WriteLine('Type\tTime\tMessage')
    return logStreamWriter



