from System.IO import Directory,Path
import JobLogic as jl
import logutils

#from sys import path
#global experiment variables to be defined by user
dataFolder = r'D:\tempDat\AutoMic_test' #has to be existing empty folder

#advanced settings which user does not need to change
logFileName="experimentLog.txt"


if not Directory.Exists(dataFolder) or Directory.GetFileSystemEntries(dataFolder).Count>0:
    print('Data folder is not Specified correctly')
    exit(-1)
    pass
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

imagingJobList = list()
imagingJobList.append(jl.ImagingJob('LZ','Low.Zoom'))
imagingJobList.append(jl.ImagingJob('HZ','High.Zoom'))

for job in imagingJobList:
    Directory.CreateDirectory(job.getPipelineName())

logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)


raw_input('Press enter to continue: ')