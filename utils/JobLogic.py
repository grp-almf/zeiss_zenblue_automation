﻿class ImagingJob:
    """class to specify Job Parameters"""
    def __init__(self,zenJobName,pipelineJobName):
        self.zenJobName=zenJobName
        self.pipelineJobName=pipelineJobName
        self.zOffset=0
        self.waitForFeedback=False

    def __init__(self,zenJobName,pipelineJobName,zOffset):
        self.zenJobName=zenJobName
        self.pipelineJobName=pipelineJobName
        self.zOffset=zOffset

    def __init__(self,zenJobName,pipelineJobName,zOffset,waitForFeedback=False):
        self.zenJobName=zenJobName
        self.pipelineJobName=pipelineJobName
        self.zOffset=zOffset
        self.waitForFeedback=waitForFeedback


    def getZenName(self):
        return self.zenJobName

    def getPipelineName(self):
        return self.pipelineJobName

    def getZOffset(self):
        return self.zOffset
    
    def getWaitForFeedback(self):
        return self.waitForFeedback
