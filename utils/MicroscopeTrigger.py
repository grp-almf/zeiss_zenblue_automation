﻿def runAcquisitionJobOffset(jobName,zen, dx, dy, dz):
    exp = zen.Acquisition.Experiments.GetByName(jobName)
    print type (exp)
    print dir (exp)
    exp.SetActive()
    #job.ClearTileRegionsAndPositions(0)
    #job.AddSinglePosition(0,zen.Devices.Stage.ActualPositionX+dx,zen.Devices.Stage.ActualPositionY+dy,zen.Devices.Focus.ActualPosition+dz)
    print zen.Devices.Focus.ActualPosition, dz
    success=zen.Acquisition.Execute(exp)
    #job.ClearTileRegionsAndPositions(0)
    return success




def runAcquisitionTileJobOffset(jobName, dx, dy, dz):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    job.ClearTileRegionsAndPositions(0)
    job.AddRectangleTileRegion(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,607,607,Zen.Devices.Focus.ActualPosition+dz)
    success = Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success


def runBleachingExperiment(jobName,zen,regionList):
    job = zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    print dir(job)
    zen.Acquisition.Experiments.ActiveExperiment.ClearTileRegionsAndPositions(0)
    #job.AddSinglePosition(0,zen.Devices.Stage.ActualPositionX+dx,zen.Devices.Stage.ActualPositionY+dy,zen.Devices.Focus.ActualPosition+dz)
    #job.ClearExperimentRegionsAndPositions(0)
    for bleachRegion in regionList:
        thisImage=zen.Application.ActiveDocument
        #print thisImage
        scaling=thisImage.Scaling
        topLeftStagePoint=job.GetStageTopLeftOfImage(scaling,thisImage)
        #print topLeftStagePoint
        job.AddEllipseExperimentRegion(scaling,topLeftStagePoint,0,regionList.x,regionList.x,regionList.radius,regionList.radius,Colors.Pink,False,True,True)

    success=zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    return success
