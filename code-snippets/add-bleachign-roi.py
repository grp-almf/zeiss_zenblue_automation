﻿thisExperiment=Zen.Acquisition.Experiments.ActiveExperiment

thisImage=Zen.Application.ActiveDocument
#print thisImage

scaling=thisImage.Scaling

topLeftStagePoint=thisExperiment.GetStageTopLeftOfImage(scaling,thisImage)

#print topLeftStagePoint

thisExperiment.ClearExperimentRegionsAndPositions(0)

thisExperiment.AddEllipseExperimentRegion(scaling,topLeftStagePoint,0,100,100,10,10,Colors.Pink,False,True,True)