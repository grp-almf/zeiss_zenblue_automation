﻿#reset all global variables
#from sys import path
#global experiment variables to be defined by user
rootFolder = r'D:\USERS\Tina\2023-05-31' #has to be existing folder
nTimePointsImaging=1
timeIntervalImaging=600
timeIntervalPumping=1200
pumping=False

#advanced settings which user does not need to change
logFileName="experimentLog.txt"
pumpPortName='COM15'
defaultPositionsJobName='Tina-defaultpositions'
markerSize=10




import subprocess
import json
import sys
from System.IO import Directory,Path,File
from datetime import datetime
import zeiss_zenblue_automation.utils.JobLogic as jl
import zeiss_zenblue_automation.utils.logutils as logutils
import zeiss_zenblue_automation.utils.pathutils as pathutils
import time
from System.IO.Ports import SerialPort
#import ExperimentRunFunctions

#defining jobs
imagingJobs = {'AF':               jl.ImagingJob('Tina-AF-20x-dry','AF',0),
               'LowZoom':          jl.ImagingJob('Tina-3colortracks','Selection',0),
               'Photomanipulation':jl.ImagingJob('Tina-Photoconversion-A647','PA',0)
               }
               


#checking specified root folder
if not Directory.Exists(rootFolder):
    print('Data folder is not Specified correctly')
    exit(-1)
    pass

dataFolder=Path.Combine(rootFolder,datetime.now().strftime('%Y%m%d-%H%M%S'))
Directory.CreateDirectory(dataFolder)
Directory.SetCurrentDirectory(dataFolder)
logutils.writeInfoLogMessage(messageText="Root folder checked",logFilePath=logFileName)

#Initialise pumping
def sendPumpingSignal(ser):
    ser.Open()
    ser.Write('start\r')
    ser.Close()

if pumping:
    ser=SerialPort(pumpPortName,9600)
    #ser.Open()
    logutils.writeInfoLogMessage(messageText="Trying to connect to the pump: %s" %ser.IsOpen,logFilePath=logFileName)




#creating subfolders to save jobs data
for jkey in imagingJobs.keys():
    Directory.CreateDirectory(imagingJobs[jkey].getPipelineName())
#Directory.CreateDirectory(imagingJobs['LowZoom'].getPipelineName()+"_max")
logutils.writeInfoLogMessage(messageText="Data subdirectories created",logFilePath=logFileName)

#get list of pre-defined positions
positionsExperiment=Zen.Acquisition.Experiments.GetByName(defaultPositionsJobName)
positions=positionsExperiment.GetSinglePositionInfos(0)
nPositions=len(positions)
print('Number of positions identified:  %d' %nPositions)


def runAcquisitionJobOffset(jobName, dx, dy, dz):
    exp = Zen.Acquisition.Experiments.GetByName(jobName)
    print 'job=%s dz=%f' %(jobName,dz)
    
    exp.SetActive()
    
    exp.ClearTileRegionsAndPositions(0)
    exp.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    #print zen.Devices.Focus.ActualPosition, dz
    success=Zen.Acquisition.Execute(exp)
    if success:
            logutils.writeInfoLogMessage(messageText="Acquired image %s at position X=%f; Y=%f; Z=%f" %(jobName,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz),logFilePath=logFileName)
    exp.ClearTileRegionsAndPositions(0)
    return success
    
def runBleachingExperiment(jobName,feedback):
    job = Zen.Acquisition.Experiments.GetByName(jobName)
    job.SetActive()
    Zen.Acquisition.Experiments.ActiveExperiment.ClearTileRegionsAndPositions(0)
    #job.AddSinglePosition(0,Zen.Devices.Stage.ActualPositionX+dx,Zen.Devices.Stage.ActualPositionY+dy,Zen.Devices.Focus.ActualPosition+dz)
    job.ClearExperimentRegionsAndPositions(0)
    thisImage=Zen.Application.ActiveDocument
    #print thisImage
    scaling=thisImage.Scaling
    topLeftStagePoint=job.GetStageTopLeftOfImage(scaling,thisImage)
    getBleachRois(feedback)
    #for bleachRegion in regionList:
    #    #print topLeftStagePoint
    #    job.AddEllipseExperimentRegion(scaling,topLeftStagePoint,0,bleachRegion['X'],bleachRegion['Y'],bleachRegion['RADIUS'],bleachRegion['RADIUS'],Colors.Pink,False,True,True)
    success=Zen.Acquisition.Execute(job)
    job.ClearTileRegionsAndPositions(0)
    
    return success
    
def saveAndGetFeedback(image,path):
    Zen.Application.Save(image,path)
    logutils.writeInfoLogMessage(messageText="Recorded and saved image %s" %path,logFilePath=logFileName)
    feedbackPath=pathutils.getDecisionPath(path)
    while not File.Exists(feedbackPath):
        print 'Waiting for decision file'
        time.sleep(1)
    
    try:
        f=open(feedbackPath,'r')
        decisionString=f.read()
        f.close()
        decisionDictionary=json.loads(decisionString)
        logutils.writeInfoLogMessage(messageText="Decision read for image %s" %path,logFilePath=logFileName)
    except:
        logutils.writeErrorLogMessage(messageText="Can not read decision for image %s" %path,logFilePath=logFileName)
        decisionDictionary=None
    
    return decisionDictionary
    

def getAutofocusOffsetMicrons(image,decisionDictionary):
    try:
        if decisionDictionary is not None:
            return (decisionDictionary['POSITIONS'].values()[0]['Z']-(image.Bounds.SizeZ-1)/2.0)*image.Scaling.Z
        else:
            logutils.writeErrorLogMessage(messageText="Decision dictionary is None",logFilePath=logFileName)
            return 0
    except:
        logutils.writeErrorLogMessage(messageText="Can not interpret decision dictionary",logFilePath=logFileName)
        return 0
        

def getHZPositions(image,decisionDictionary):
    xValues=decisionDictionary['X']
    yValues=decisionDictionary['Y']
    
    positionsPixels=list()
    positionsStage=list()
    for i in range(len(xValues)):
        positionsPixels.append((xValues[i],yValues[i]))
        xStage=(xValues[i]-(image.Bounds.SizeX-1)/2.0)*image.Scaling.X
        yStage=(yValues[i]-(image.Bounds.SizeY-1)/2.0)*image.Scaling.Y
        positionsStage.append((xStage,yStage))
    return positionsStage,positionsPixels
    
def getBleachRois(decisionDictionary):
    thisExperiment=Zen.Acquisition.Experiments.ActiveExperiment
    thisImage=Zen.Application.ActiveDocument
    scaling=thisImage.Scaling

    topLeftStagePoint=thisExperiment.GetStageTopLeftOfImage(scaling,thisImage)

    thisExperiment.ClearExperimentRegionsAndPositions(0)

    jRois=decisionDictionary['ROIS']
    for jr_key in jRois.keys():
        jr=jRois[jr_key]
        if jr["TYPE"]=="POLYGON":
            pList=list()
            jPositions=jr["POSITIONS"]
            for pKey in sorted(jPositions.keys()):
                p=dict()
                jPosition=jPositions[pKey]
                p['X']=jPosition['X']
                p['Y']=jPosition['Y']
                
                pList.append((p['X'],p['Y']))

            thisExperiment.AddPolygonExperimentRegion(scaling,topLeftStagePoint,0,pList,Colors.Pink,False,True,True)

        if jr["TYPE"]=="OVAL":
            radiusX=jr["WIDTH"]/2
            radiusY=jr["HEIGHT"]/2
            centerX=jr["X"]+radiusX
            centerY=jr["Y"]+radiusY

            thisExperiment.AddEllipseExperimentRegion(scaling,topLeftStagePoint,0,centerX,centerY,radiusX,radiusY,Colors.Pink,False,True,True)


def markSelectedPositions(image,positions):


    for position in positions:
        circle1 = image.Graphics.Add(ZenGraphicCategory.Circle)
        circle1.SetBounds(position[0]-markerSize/2, position[1]-markerSize/2, markerSize, markerSize)
        circle1.Thickness = 1
        circle1.StrokeColor = ZenColors.White
        circle1.IsMeasurementVisible = False    

    return

def setBleachingRegions(bleachPositions):
    thisExperiment=Zen.Acquisition.Experiments.ActiveExperiment
    thisImage=Zen.Application.ActiveDocument
    scaling=thisImage.Scaling

    topLeftStagePoint=thisExperiment.GetStageTopLeftOfImage(scaling,thisImage)

    thisExperiment.ClearExperimentRegionsAndPositions(0)

    for position in bleachPositions:
        thisExperiment.AddEllipseExperimentRegion(scaling,topLeftStagePoint,0,position[0],position[1],10,10,Colors.Pink,False,True,True)


def runDefaultPositionAcquisition(positionIndex, positionX,positionY,positionZ):

    acquiredImages=list()
    
    #acquire autofocus
    defaultExperimentName=imagingJobs['AF'].getZenName()
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, 0)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['AF'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    Zen.Application.Save(acquiredImage,savePath)
    #Zen.Application.Documents.Add(acquiredImage)
    autofocusOffset=getAutofocusOffsetMicrons(acquiredImage,saveAndGetFeedback(acquiredImage,savePath))#getAutofocusOffset(acquiredImage)
    acquiredImages.append(acquiredImage)
    print autofocusOffset
    
    #acquire lowZooom Image
    
    defaultExperimentName=imagingJobs['LowZoom'].getZenName()
    
    Zen.Devices.Stage.MoveTo(positionX,positionY)
    Zen.Devices.Focus.MoveTo(positionZ)
    acquiredImage=runAcquisitionJobOffset(defaultExperimentName, 0, 0, autofocusOffset+imagingJobs['LowZoom'].getZOffset())
    logutils.writeInfoLogMessage(messageText="Recorded image  %s" %savePath,logFilePath=logFileName)
    acquiredImages.append(acquiredImage)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['LowZoom'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    
    
    #Zen.Application.Save(acquiredImage,savePath)
    ##process LowZoom Image
    feedback=saveAndGetFeedback(acquiredImage,savePath)
    print feedback
    
    defaultExperimentName=imagingJobs['Photomanipulation'].getZenName()
    Zen.Devices.Focus.MoveTo(positionZ+autofocusOffset+imagingJobs['Photomanipulation'].getZOffset())
    acquiredImage=runBleachingExperiment(defaultExperimentName,feedback)
    savePath=pathutils.generateSavePath(dataFolder,imagingJobs['Photomanipulation'].getPipelineName(),0,positionIndex+1,timeIndex+1)
    Zen.Application.Save(acquiredImage,savePath)


    logutils.writeInfoLogMessage(messageText="Recorded image %s" %savePath,logFilePath=logFileName)
    acquiredImages.append(acquiredImage)
    try:
        Zen.Application.Documents.RemoveAll(False)
    except:
        logutils.writeWarningLogMessage(messageText="Problem with closing files",logFilePath=logFileName)
    

    #for image in acquiredImages:
    #    try:
    #        image.Close()
    #    except:
    #        logutils.writeWarningLogMessage(messageText="Problem with closing file",logFilePath=logFileName)
    
    pass
    return



previousPumptime=datetime(1970,1,1)
previousImagingTime=datetime(1970,1,1)
print "Now %s" %datetime.now()
print "Previous  %s" %previousImagingTime
for timeIndex in range(0,nTimePointsImaging):
    try:
        while (datetime.now()-previousImagingTime).total_seconds()<timeIntervalImaging:
            print 'Time to wait %d s' %(timeIntervalImaging-(datetime.now()-previousImagingTime).total_seconds())
            time.sleep(1)
        previousImagingTime=datetime.now()
        
        
        print "Do it!"
        for positionIndex in range(0,nPositions):
            try:
                p=positions[positionIndex]
                runDefaultPositionAcquisition(positionIndex=positionIndex, positionX=p.X,positionY=p.Y,positionZ=p.Z)
            except Exception, e:
                logutils.writeErrorLogMessage(messageText="Unclassified error when running acquisition in one of default positions error message: %s" %(str(e)),logFilePath=logFileName)
            
            if pumping and ((datetime.now()-previousPumptime).total_seconds()>timeIntervalPumping):
                sendPumpingSignal(ser)
                previousPumptime=datetime.now()
                logutils.writeInfoLogMessage(messageText="Send pumping signal",logFilePath=logFileName)
                time.sleep(10)
    except Exception , e: 
        logutils.writeErrorLogMessage(messageText="Unclassified error when running main loop. Error message: %s" %(str(e)),logFilePath=logFileName)
        exit(-1)


print('Pipeline finished')
logutils.writeInfoLogMessage(messageText="Pipeline finished",logFilePath=logFileName)
