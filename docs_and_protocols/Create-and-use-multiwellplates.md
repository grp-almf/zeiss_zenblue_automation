This protocol describes to make use of "sample carriers" in ZebLue Software for"Default positions" acquisition Job.

This can be used to add well names to the names of acquired files. 

# Creating the plate template

Note - procedure can code were tested with 96 well plates, but can be used for other multiwell and multichamber well formats.

1. Create the jobs for specifying positions and add at least 1 track. Imaging settings do not need to be the same as in actual imaging jobs, but size of the field of view should match to actual (low zoom settings). This is required to get correct spacing between acquisition points when specifying points array.
2. Add 1 support point at the center of each well.


Note: to select all wells keep "Shift" and click left top well (A1) and bottom right well (H12). Selected wells are shown in Blue.
Note: If you recalibrate the carrier at kater time point, removesupport points and add them again.

3. Select default positions for feedback microscopy experiment using either tool for individual positions, position arrays or based on the sample carrier (any of 3 highlighted tools can be used).
